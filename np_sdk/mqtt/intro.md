# MQTT

NP_MQTT is another way to communicate with devices.

### Commands
- Same format as P2P method. Please refer to [cmds](../cmds/intro.md).

### Initialization
- APP should initialize the MQTT channel before sending commands.
  - app
    - app key
  - did
    - target device id
  - token
    - A string provided by cloud after logining
  - uid
    - A string provided by cloud after logining
  - passwd
    - password that user input for logining
    
```C
  typedef union {
    struct {
    } dev;
    struct {
      char app[52];
      char uid[52];
      char token[512];
      char did[52];
      char passwd[128];
    } app;
  } NP_MQTT_PARAM;

  int
  np_mqtt_init (
    NP_MQTT_PARAM  *param
    );

  void
  app_mqtt_init (
    void
    )
  {
    NP_MQTT_PARAM mqtt_param;

    strcpy (mqtt_param.app.app, "NAPPAQJH67L7EIUJUPBS98N0T9M6PCDJ");
    strcpy (mqtt_param.app.did, "KYHPBCSMO27QCF4IDMBR6GNUGITKK43K");
    strcpy (mqtt_param.app.token, "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IjliMmVlZWQ3MDBhNWUzNmFiYzNhMjNjZmU2YzM5NmM3IiwiZXhwIjoxNjIyMTk3MTEyLCJpc3MiOiJOb3RpZnlJbmZvLk5ldCJ9.aB1JfY2bzEuS2FMUU0vkHjwiUDoE1IOuy1zD9htFyQY");
    strcpy (mqtt_param.app.uid, "NXPUDO120KS3602ICKA0JCS1AAQ3V933");
    strcpy (mqtt_param.app.passwd, "st310006");

    np_mqtt_init (&mqtt_param);
  }
```

### Sending Commands
- APP should specify a did & command formatted as json string
```C
int
np_mqtt_cmd_send (
  char  *did,
  char  *json_str
  );
```

### Destroy MQTT channel
- Simply call np_mqtt_destroy() with device id
```C
void
np_mqtt_destroy (
  char *did
  );
```
- Or call np_mqtt_destroy_all() to destroy all clients
```C
void
np_mqtt_destroy_all (
  void
  );
```
