# Airtouch

### APP
App should call the following APIs:

```C
char *
airtouch_spreader_start (
  char *interface,
  char *ssid,
  char *pwd,
  char *token,
  int   process_times,
  int   packet_gap
  );

void
airtouch_spreader_exit (
  void
  );
```

- Step 1 : Call ```airtouch_spreader_start ()```
  - This API will auto generate the [message](intro.md#message). APP dose not need to bother that.
  - interface
    - name of wifi interface ex: wlan0
  - ssid
    - SSID of wifi
  - pwd
    - password of SSID
  - token
    - string from cloud
  - process_times
    - times for sending whole message
    - set to 3
  - packet_gap
    - delay for each packet
    - set to 2
- Step 2 : waiting for response
  - Device will resonpse after connecting to the SSID that APP assigned.
  - APP should poll the cloud at the same time in case that the message from device is not received.
- Step 3 : Call ```airtouch_spreader_exit ()```
  - Once APP receives the message ```{"rsp":0}``` from the device or the cloud return a success status for registering the device. APP should call ```airtouch_spreader_exit ()``` to release the resouces that ```airtouch``` occupied.
