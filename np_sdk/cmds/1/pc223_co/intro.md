# PC223 - ClockworkOrange

  * [SetGet Type Table](../intro.md#setget_table)

### Common SLOT Table
| Code | Statement | Type | Access Type | Value | Default Value |
| :-: | :-: | :-: | :-: | :- | :-: |
| 101 | Light | Boolean | 0 | - | ON |
| 103 | Flip | Boolean | 0 | - | OFF |
| 104 | Watermark | Boolean | 0 | - | ON |
| 106 | AlarmSensitivity | Enum | 0 | 0: LOW <br /> 1: MID <br /> 2: HIGH | 2 |
| 108 | NightMode | Enum | 0 | 0: OFF <br /> 1: ON | 0 |
| 109 | SdcardStorageOnlyGet | String | 2 | Total/Used/Free | 0/0/0 |
| 110 | SdcardStatusOnlyGet | Integer | 2 | 1: normal <br /> 2: abnormal <br /> 3: no space <br /> 4: formating <br /> 5: no SD | 5 |
| 111 | SdcardFormat | Boolean | 0 | - | OFF |
| 112 | SdcardUnmount | Boolean | 0 | - | OFF |
| 150 | SdcardRecordEnable | Boolean | 0 | - | ON |
| 151 | SdcardRecordMode | Enum | 0 | 0: Record By Event<br /> 1: Record Continuously | 0 |
| 162 | DeviceReboot | Boolean | 1 | - | OFF |
| 168 | MotionZoneSwitch | Boolean | 0 | - | ON |
| 169 | MotionZoneArea | String | 0 | {\\\\\\"num:1\\\\\\",\\\\\\"region0\\\\\\":{\\\\\\"x\\\\\\":0, \\\\\\"y\\\\\\":0, \\\\\\"xlen\\\\\\":50,\\\\\\"ylen\\\\\\":50}} | {\\\\\\"num:1\\\\\\",\\\\\\"region0\\\\\\":{\\\\\\"x\\\\\\":0, \\\\\\"y\\\\\\":0, \\\\\\"xlen\\\\\\":100,\\\\\\"ylen\\\\\\":100}} |
| 181 | PIR1Switch | Boolean | 0 | - | ON |
| 184 | PIRSensitivity | Enum | 0 | 0: LOW <br /> 1: MID <br /> 2: HIGH | 0 |
| 256 | FactoryReset | Boolean | 0 | - | OFF |
| 257 | DevOtaStart | Boolean | 0 | - | OFF |
| 260 | GetFwVer | String | 2 | - | 0.0.0 |
| 261 | GetVidChannelInfo | String | 2 | Channel/Width/Height/FPS;... <br /> 0/1920/1080/20;1/640/360/15; | - |
| 262 | WifiSignalStrength | Integer | 2 | - | 0 (0~100) |
| 267 | SetGetSdpInfo | String | 0 | Set: APP/Cloud will provide SDP info <br /> Get: Device should provide self SDP info | - |
| 274 | Brightness | Integer | 0 | 0 ~ 100 | - |
| 288 | WIFI SSID | String | 2 | - | - |
| 289 | SetOffer | String | 1 | - | - |
| 290 | SetCandidate | String | 1 | - | - |
| 291 | ReportCandidate | String | 2 | - | 0 |
| 296 | List Event Months | String | 2 | EX: ["2022/10", "2022/12", "2023/01"] | - |
| 297 | List Event Dates | String | 3 | Set: "yyyy/mm" -> "2023/01" <br/><br/> Response: ["2022/12/01", "2022/12/12", "2022/12/25"] | - |
| 298 | List Event Files | String | 3 | Set: "yyyy/mm/dd" -> "2023/01/05" <br/><br/> Response: ["15_34_33.mp4;1234567", "15_38_33.mp4;1234567"]  | - |

---

### Customized SLOT Table
| Code | Statement | Type | Access Type | Value | Default Value |
| :-: | :-: | :-: | :-: | :- | :-: |
| 500 | Upload Event File | String | 1 | Set: "filename;upload-url" -> "2023/01/05 15_34_33.mp4; https://xxx.xxx.xxx.xxx/xxx" <br/><br/> Media files should post to url as "multipart/form-data" <br/><br/> File name should be "MAC12address_yyyymmdd_hhmmss.mp4" | - |
| 501 | Delete Event File | String | 1 | Set: "filename1;filename2;filename3" -> "2022/10/05 15_33_33.mp4;2022/10/05 15_38_33.mp4" | - |
| 502 | Record Event | String | 1 | Set: "duration;upload-url" -> "30;https://xxx.xxx.xxx.xxx/xxx" <br/><br/> File name should be "MAC12address_yyyymmdd_hhmmss.mp4" | 100 |
| 503 | Set Alarm Notification | String | 1 | Set: "enable;record;duration;notify-url" <br/><br/> enable: 轉成數字 0 or 1, 表示是否啟動 Alarm 觸發 <br/> record: 轉成數字 0 or 1, 表示是否在警報觸發時自動錄影 <br/> duration: 轉成數字 10~300 (秒), 表示包含預錄的錄影時間長度 <br/> notify-url: <br/> 為警報觸發時的通知網址 (POST), 內容包含:類型、設備 ID、MAC 地址、時間 <br/> { <br/> "type": "alarm", <br/> "did": "XWZP0KR....", // 32-digit <br/> "mac": XX:XX:XX:XX:XX:XX", <br/> "time": "yyyy/mm/dd hh:mm:ss" // UTC <br/> } <br/> 若有設定自動錄影, 當錄影結束時再次通知:類型、設備 ID、MAC 地址、時間、檔案名稱 { <br/> "type": "alarm", <br/> "did": "XWZP0KR....", // 32-digit <br/> "mac": XX:XX:XX:XX:XX:XX", <br/> "time": "yyyy/mm/dd hh:mm:ss" // UTC <br/> "fname": "xxxxxx.mp4" <br/> } | 0 |
| 504 | Set Status Notification | String | 1 | Set: "enable;notify-url" <br/><br/> enable: 轉成數字 0 or 1, 表示是否啟動狀態通知 <br/> notify-url: 為設備狀態變更的通知網址 (POST), 時機包括 設備上線、Agent 更新、SD 卡狀態 <br/><br/> 設備上線的通知:類型、設備 ID、MAC 地址、時間、韌體版本、Agent 版本、SD 卡狀態 <br/> { <br/>  "type": online", <br/> "did": "XWZP0KR....", // 32-digit <br/> "mac": "XX:XX:XX:XX:XX:XX", <br/> "time": "yyyy/mm/dd hh:mm:ss", // UTC <br/> "fwver": "1.0.0", // firmware ver. <br/> "agent": "1.0.0", // agent ver. <br/> "sdcs": "sdXXXXX" // sd-card state <br/> } <br/> Agent 更新完成的通知:類型、設備 ID、MAC 地址、時間、韌體版本、Agent 版本 <br/> { <br/>  "type": agentUpdated", <br/> "did": "XWZP0KR....", // 32-digit <br/> "mac": "XX:XX:XX:XX:XX:XX", <br/> "time": "yyyy/mm/dd hh:mm:ss", // UTC <br/> "fwver": "1.0.0", // firmware ver. <br/> "agent": "1.0.1", // agent ver. <br/> "sdcs": "sdXXXXX" // sd-card state <br/> } <br/> SD 卡狀態變更的通知:類型、設備 ID、MAC 地址、時間、韌體版本、Agent 版本 <br/> { <br/>  "type": sdXXXXX", // sdInsert or sdExtract or sdError <br/> "did": "XWZP0KR....", // 32-digit <br/> "mac": "XX:XX:XX:XX:XX:XX", <br/> "time": "yyyy/mm/dd hh:mm:ss", // UTC <br/> "fwver": "1.0.0", // firmware ver. <br/> "agent": "1.0.1", // agent ver. <br/> } <br/> | 100 |
| 505 | Update Agent | String | 1 | Set: "agent-file-url" <br/><br/>  agent-file-url: 為下載網址, 取得 Agent 程式的 raw-data 檔案, 其中檔案標頭包含版本資訊 | - |

---

### Agent OTA File Format
| Byte Position | Name | Length | Value | Description |
| :-: | :- | :-: | :-: | :- |
| 0 | Header 0 | 1 | 0xFE | - |
| 1 | Header 1 | 1 | 0xEF | - |
| 2 | Version String Length | 1 | Varies | - |
| 3 | Version String | N | Varies | Ex: 1.2.3 |
| X* | MD5 String of Agent File | 32 | Varies | Ex: 08e74e63d60d6a662018dcdd5f3218b6 |
| Y*  | Agent File Size | 4 | Varise | Size should be in big endian. |
| Z* | Agent File | N | Varies | Raw data |

* Let "3 + Version String Length" = X
* Let "X + 32" = Y
* Let "Y + 4" = Z

---
