# 1010 - GetPushButton

### Command Number
| Command Nunber |
| :-:            |
| 1010              |

### Host To Device
- Packet Format

Host To Device                          |     |     |     |     |
:-:                                     | :-: | :-: | :-: | :-: | -
Leading Byte | Type | Length | Data                       | End Byte          
0xFE         | 0    | Varies |  See Example Below         | 0xEF

- Example Data

```json
{
  "cmd": 1010
}
```

***

### Device To Host
- Packet Format

Device To Host                          |     |     |     |     |
:-:                                     | :-: | :-: | :-: | :-: | -
Leading Byte | Type | Length | Data                       | End Byte          
0xFE         | 0    | Varies |  See Example Below         | 0xEF

- Example Data

```json
{
  "cmd": 1010,
  "data": {
    "resultType": 2,
    "result": "Success" // Or other description
  }
}
```