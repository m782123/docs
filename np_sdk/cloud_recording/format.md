# Media File Format

## Packet Type

| Value | Statement |
| - | - |
| 0x00 | I frame |
| 0x01 | P frame |
| 0x02 | U-law |
| 0x03 | PCM |
| 0x04 | Following 4 bytes are integer in little endian |
| 0xF0 | End of packet |

---

## Packet Header

| 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 ~ 19 | 20 ~ N |
| - | - | - | - | - | - | - | - | - | - | - | - |
| Header 0 | Header 1 | Length HH |  Length HL | Version | Reserved | Reserved | Length LH | Length LL | Reserved | FPS | Media data |
| 0x55     | 0xAA     | Vary      | Vary       | 0x02    | 0x00     | 0x00     | Vary      | Vary | 0x00 | Vary | Vary | 

- Packet position >= 10 should be payload. The payload length should be :
  - ```c
    ((pkt[pktPosLenHH]) << 24) | ((pkt[pktPosLenHL]) << 16) | ((pkt[pktPosLenLH]) << 8) | ((pkt[pktPosLenLL]));
    ```

---

## Payload - FPS

| 10 | 11 ~ 14 | 15 | 16 ~ 19 |
| - | - | - | - |
| Type | Video FPS | Type | Audio FPS |
| 0x04 | Vary  | 0x04 | Vary |

---

## Payload - Media data

- Packet position >= 20 should be media data, they all share the same format:

| 0 | 1 | 2 ~ 5 | 6 ~ N |
| - | - | - | - |
| Type | Type | Data Length | Raw data |
| Vary | 0x04 | Vary | Vary |