# 1007 - GetAudioStream

### Command Number
| Command Nunber |
| :-:            |
| 1007              |

### Host To Device
- Packet Format

Host To Device                          |     |     |     |     |
:-:                                     | :-: | :-: | :-: | :-: | -
Leading Byte | Type | Length | Data                       | End Byte          
0xFE         | 0    | Varies |  See Example Below         | 0xEF

- Example Data

```json
{
  "cmd": 1007,
  "data": {
    "paramType": 0,
    "param": 8 // in seconds
  }
}
```

***

### Device To Host
- Packet Format

Device To Host                          |     |     |     |     |
:-:                                     | :-: | :-: | :-: | :-: | -
Leading Byte | Type | Length | Data                       | End Byte          
0xFE         | 1    | Varies |  See Example Below         | 0xEF

- Example Data
  - Return Raw Data