# 1 - Set / Get SLOTS

* [Response Table](../intro.md#rsp_table)

### Action Table
| Code | Statement |
| :-: | :-: |
| 0 | Get |
| 1 | Set |
| 2 | Get all slots in device |

---

### Type Table
| Code | Statement | Json property |
| :-: | :- | :- |
| 0 | Boolean | boolVal |
| 1 | Integer | intVal |
| 2 | String | strVal |
| 3 | Enum | enumVal & enumName |
| 4 | Raw | rawVal |

---

### Access Type {#id=setget_table}
| Code | Statement |
| :-: | :-: |
| 0 | Set & Get |
| 1 | Set Only |
| 2 | Get Only |
| 3 | Response After Set |

---

### Raw packet format

| Position | Name | Value |
| :-: | :-: | :-: |
| 0 | Header 0 | 0x55 |
| 1 | Header 1 | 0xAA |
| 2 | Packet Len 1 (High High) | Varies |
| 3 | Packet Len 2 (High Low) | Varies |
| 7 | Packet Len 3 (Low High) | Varies |
| 8 | Packet Len 4 (Low Low) | Varies |
| 10 | Packet Start | Varies |
| N - 6 | Packet End | Varies |
| N - 5 | CRC32 (High High) | Varies |
| N - 4 | CRC32 (High Low) | Varies |
| N - 3 | CRC32 (Low High) | Varies |
| N - 2 | CRC32 (Low Low) | Varies |
| N - 1 | Footer 0 | 0xAA |
| N | Footer 1 | 0x55 |

* The packet length is the length from position 0 ~ (N - 6), means from "Header 0" to "Packet End".
* CRC32 should be the checksum from position 0 ~ (N - 6), means from "Header 0 to "Packet End".
* Position (N - 5) ~ N will present when the communication protocol is unreliable. (e.g. UART)

---

### Note for Cloud
The MQTT packets that send/receive from APPs/Devices side are all converted to data bytes (another format that parse from the original json string) with two prefix bytes 0x55 & 0xAA. They represent the data format is in data bytes (not json string). If cloud tries to send json string without specific prefix bytes, device will respond nothing. So please add prefix bytes 0x55, 0xCC brefore the json string.

| Format | Byte 0 | Byte 1 | Byte N |
| :-: | :-: | :-: | :-: |
| Raw bytes | 0x55 | 0xAA | bytes ... |
| JSON String | 0x55 | 0xCC | JSON string ... |
| JSON String | 0x55 | 0xEE | JSON string ... |

---

### Raw Packet for getting slot 303

APP / client should send packet (Get single slot 303) to devices. Then APP / client will receive respond from device. 

| Direction | Bytes |
| :-: | :-: |
| Send to Device | 0x55, 0xAA, 0x00, 0x00, 0x01, 0x7F, 0x00, 0x00, 0x21, 0x34, 0x00, 0x15, 0x00, 0x01, 0x05, 0x14, 0x00, 0x0F, 0x14, 0x00, 0x07, 0x15, 0x01, 0x2F, 0xF0, 0xF0, 0x10, 0x16, 0x00, 0x01, 0xE2, 0x40, 0xF0 |
| Receive from Device | 0x55, 0xAA, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x4D, 0x00, 0x00, 0x15, 0x00, 0x01, 0x0F, 0x14, 0x00, 0x07, 0x15, 0x01, 0x2F, 0x08, 0x14, 0x02, 0x0B, 0x02, 0x15, 0x00, 0x21, 0x44, 0x6F, 0x54, 0x77, 0x32, 0x65, 0x53, 0x59, 0x5A, 0x49, 0x6D, 0x4D, 0x4C, 0x31, 0x6F, 0x49, 0x49, 0x54, 0x4C, 0x42, 0x56, 0x6E, 0x67, 0x74, 0x79, 0x57, 0x47, 0x6D, 0x73, 0x50, 0x32, 0x33, 0x00, 0xF0, 0xF0, 0x10, 0x16, 0x00, 0x01, 0xE2, 0x40, 0x02, 0x16, 0x00, 0x00, 0x00, 0x00, 0xF0 |

---

### AP mode 

#### Device will be a TCP server, clients should connect to device by the following IP & port:

| IP | PORT |
| :-: | :-: |
| 192.168.100.1 | 38790 |

#### The packet should be formatted as following:
| Format | Byte 0 | Byte 1 | Byte N |
| :-: | :-: | :-: | :-: |
| JSON String | 0x55 | 0xCC | JSON string ... |

#### The packet should be packed as following

| Position | Name | Value |
| :-: | :-: | :-: |
| 0 | Header 0 | 0xFE |
| 1 | Header 1 | 0xED |
| 2 | Len 1 (High High) | Varies |
| 3 | Len 2 (High Low) | Varies |
| 4 | Len 3 (Low High) | Varies |
| 5 | Len 4 (Low Low) | Varies |
| 6 | CRC32 1 (High High) | Varies |
| 7 | CRC32 2 (High Low) | Varies |
| 8 | CRC32 3 (Low High) | Varies |
| 9 | CRC32 4 (Low Low) | Varies |
| 10 | Packet Start | Varies |
| N - 2 | Packet End | Varies |
| N - 1 | Footer 0 | 0xBE |
| N | Footer 1 | 0xEF |

* Len & CRC32 should be in big-endian
* Len is the length of the packet.
* CRC32 should be the crc32 checksum of the packet.

---

### SLOT Table
The following types are the default one. It may different if cloud set to another type.

| Code | Statement | Type | Access Type | Value/Comment | Default Value (Range) |
| :-: | :-: | :-: | :-: | :- | :-: |
| 101 | Light | Boolean | 0 | - | ON |
| 103 | Flip | Boolean | 0 | - | OFF |
| 104 | Watermark | Boolean | 0 | - | ON |
| 105 | SleepMode | Boolean | 0 | - | OFF |
| 106 | AlarmSensitivity | Enum | 0 | 0: LOW <br /> 1: MID <br /> 2: HIGH | 2 |
| 108 | NightMode | Enum | 0 | 0: OFF <br /> 1: ON | 0 |
| 109 | SdcardStorageOnlyGet | String | 2 | Total/Used/Free | 0/0/0 |
| 110 | SdcardStatusOnlyGet | Integer | 2 | 1: normal <br /> 2: abnormal <br /> 3: no space <br /> 4: formating <br /> 5: no SD | 5 |
| 111 | SdcardFormat | Boolean | 0 | - | OFF |
| 112 | SdcardUnmount | Boolean | 0 | - | OFF |
| 114 | MotionDetectionTimer | String | 0 | - | 00:00/23:59 |
| 115 | MotionDetectionAlarm | Raw | 0 | - | - |
| 117 | SdcardFormatStatusOnlyGet | Integer | 2 | -2000: formatting <br /> -2001: abnormal <br /> -2002: no sdcard <br /> -2003: sdcasrd error | -2002 |
| 119 | PtzDirection | Enum | 0 | direction : <br /> 7 0 1<br /> 6 x 2 <br /> 5 4 3 | 0 |
| 134 | AlarmFunction | Boolean | 0 | - | ON |
| 135 | MotionTimerSw | Boolean | 0 | - | OFF |
| 137 | SdcardRecording | Boolean | 2 | - | ON |
| 138 | BlubSwitch | Boolean | 0 | - | OFF |
| 142 | Temperature | Integer | 2 | - | 0 |
| 149 | DeviceStatus | Boolean | 2 | - | OFF |
| 150 | SdcardRecordEnable | Boolean | 0 | - | ON |
| 151 | SdcardRecordMode | Enum | 0 | 0: Record By Event<br /> 1: Record Continuously | 0 |
| 154 | DoorBellSnap | Raw | 0 | - | - |
| 158 | BlubLevel | Integer | 0 | - | 100 (0~100) |
| 159 | SirensSound | Boolean | 0 | - | ON |
| 160 | DeviceVolume | Integer | 0 | - | 100 (0~100) |
| 161 | MotionTrackingSwitch | Boolean | 0 | - | OFF |
| 162 | DeviceReboot | Boolean | 1 | - | OFF |
| 163 | ZoomCtrl | Enum | 0 | 0: Zoom In <br /> 1: Zoom Out | 0 |
| 165 | ChimeType | Enum | 0 | 0: Mechanical <br /> 1: Electronic | 0 |
| 166 | ChimeTime | Integer | 0 | - | 10 (0~120) |
| 168 | MotionZoneSwitch | Boolean | 0 | - | ON |
| 169 | MotionZoneArea | String | 0 | {\\\\\\"num:1\\\\\\",\\\\\\"region0\\\\\\":{\\\\\\"x\\\\\\":0, \\\\\\"y\\\\\\":0, \\\\\\"xlen\\\\\\":50,\\\\\\"ylen\\\\\\":50}} <br /> <br />  num: number of regions<br /> x: coordinate<br /> y: y coordinate<br /> xlen: length of x<br /> ylen: length of y<br /><br /> ex:<br /> resolution 1920 * 1080<br /> x - 50, y - 50<br /> xlen - 30, ylen - 20<br /><br /> final region: <br /> x - 960, xlen - 576<br /> y - 540, ylen - 216 <br /> (960, 540) (1536, 540) <br />+---------------------+<br /> +---------------------+ <br /> (960, 756) (1536, 756) | {\\\\\\"num:1\\\\\\",\\\\\\"region0\\\\\\":{\\\\\\"x\\\\\\":0, \\\\\\"y\\\\\\":0, \\\\\\"xlen\\\\\\":100,\\\\\\"ylen\\\\\\":100}} |
| 170 | HumanoidFilter | Boolean | 0 | - | ON |
| 172 | LightWarnSw | Boolean | 0 | - | ON |
| 173 | LightWarnTime | Integer | 0 | - | 10 (1~300) |
| 181 | PIR1Switch | Boolean | 0 | - | ON |
| 182 | PIR2Switch | Boolean | 0 | - | ON |
| 183 | PIR3Switch | Boolean | 0 | - | ON |
| 184 | PIRSensitivity | Enum | 0 | 0: LOW <br /> 1: MID <br /> 2: HIGH | 0 |
| 194 | SirenDuration | Integer | 0 | - | 30 (5~120) |
| 195 | SirenVolume | Integer | 0 | - | 100 (0~100) |
| 198 | ObjOutline | Boolean | 0 | - | ON |
| 256 | FactoryReset | Boolean | 0 | - | OFF |
| 257 | DevOtaStart | Boolean | 0 | - | OFF |
| 258 | CustomSirenSound | Raw | 1 | Maximun file size should be less than 250KB | - |
| 259 | RemoveCustomSirenSound | Boolean | 1 | - | OFF |
| 260 | GetFwVer | String | 2 | - | 0.0.0 |
| 261 | GetVidChannelInfo | String | 2 | Channel/Width/Height/FPS;... <br /> 0/1920/1080/20;1/640/360/15; | - |
| 262 | WifiSignalStrength | Integer | 2 | - | 0 (0~100) |
| 263 | StartProvidingRtspStream | String | 2 | Device sholud response with a rtsp url | rtsp://xxx.xxx.xxx.xxx:xxx |
| 264 | StartPublishingRtspStream | String | 1 | Cloud/APP should provide a rtsp url <br /><br /> Format:<br /> RTSP_STRING;EXTENDED_TIME_IN_MINUTES; | rtsp://xxx.xxx.xxx.xxx:xxx;10; |
| 265 | StopProvidingRtspStream | Boolean | 1 | If the device receive this slot, device will shutdown stream no matter the slot is TRUE / FALSE. | - |
| 266 | StopPublishingRtspStream | Boolean | 1 | If the device receive this slot, device will shutdown stream no matter the slot is TRUE / FALSE. | - |
| 267 | SetGetSdpInfo | String | 3 | Set: APP/Cloud will provide SDP info <br /> Get: Device should provide self SDP info | - |
| 268 | ReportStatus | Enum | 0 | - | 0 |
| 269 | ReportStatusFreq | Enum | 0 | - | 0 |
| 270 | Warning Indicator | Boolean | 0 | - | OFF |
| 271 | Power | Boolean | 0 | Power control | ON |
| 272 | Color Temperature | Integer | 0 | 2200 - 5000 | - |
| 273 | Adjust Color Temperature | Integer | 1 | ±1000 | - |
| 274 | Brightness | Integer | 0 | 0 ~ 100 | - |
| 275 | Adjust Brightness | Integer | 1 | ±100 | - |
| 276 | Power Level | Integer | 0 | 0 ~ 10 | - |
| 277 | Adjust Power Level | Integer | 1 | ±10 | - |
| 278 | Speaker Volume | Integer | 0 | 0 ~ 100 | - |
| 279 | Adjust Speaker Volume | Integer | 1 | ±100 | - |
| 280 | Mute Speaker | Boolean | 0 | - | ON |
| 281 | Temperature | Integer | 2 | -50 ~ 50 | - |
| 282 | Target Temperature | Integer | 0 | 10 ~ 30 | - |
| 283 | Upper Temperature | Integer | 0 | 15 ~ 35 | - |
| 284 | Lower Temperature | Integer | 0 | 5 ~ 25 | - |
| 285 | Adjust Temperature | Integer | 1 | ±10 | - |
| 286 | Temperature Mode | Enum | 0 | 0: OFF<br /> 1: HEAT<br/> 2: COOL<br/> 3: ECO<br/> 4: AUTO<br/> 5: ON | 0 |
| 287 | Renew Device MQTT Queue | Boolean | 0 | - | OFF |
| 288 | WIFI SSID | String | 2 | - | - |
| 289 | SetOffer | String | 3 | - | - |
| 290 | SetCandidate | String | 1 | - | - |
| 291 | ReportCandidate | String | 2 | - | 0 |
| 292 | High Brightness | Integer | 0 | 50 - 100 <br /> Unit: % | 100 |
| 293 | Low Brightness | Integer | 0 | 0 - 50 <br /> Unit: % | 30 |
| 294 | High Color Temprature | Integer | 0 | 2200 - 5000 <br /> Unit: K | 5000 |
| 295 | Low Color Temprature | Integer | 0 | 2200 - 5000 <br /> Unit: K | 2200 |
| 296 | List Event Months | String | 2 | EX: ["2022/10", "2022/12", "2023/01"] | - |
| 297 | List Event Dates | String | 3 | Set: "yyyy/mm" -> "2023/01" <br/><br/> Response: ["2022/12/01", "2022/12/12", "2022/12/25"] | - |
| 298 | List Event Files | String | 3 | Set: "yyyy/mm/dd" -> "2023/01/05" <br/><br/> Response: ["01.mp4;1234567", "02.mp4;1234567"]  | - |
| 299 | Manual Recording Length | Integer | 0 | 10 - 600 <br /> Unit: Seconds | 30 |
| 300 | Manual Recording Start | Boolean | 1 | - | OFF |
| 301 | Recording Status | Boolean | 2 | True - Recording <br /> False - Available for recording | OFF |
| 302 | AID | String | 2 | - | 0 |
| 303 | Command Mechanism | String | 2 | - | DoTw2eSYZImML1oIITLBVngtyWGmsP23 |
| 304 | Set / Get device time | String | 0 | Set: "TIMEZONE,UTC TIME" <br/> Example: +8,2024/02/22 17:16:30 | - |
| 305 | Device Mic Volume | Integer | 0 | 0 - 100 <br /> Unit: Percentage | 100 |
| 500+ | Reserved For Customization | - | - | - | - |

---

### SLOT Table for factory mode
- *Get Operation*

| Code | Statement | Type | Access Type | Value/Comment | Default Value (Range) |
| :-: | :-: | :-: | :-: | :- | :-: |
| 5000 | getMicVol | Integer | 2 | 0 ~ 100 | - |
| 5001 | getSpeakerVol | Integer | 2 | 0 ~ 100 | - |
| 5002 | getPic | Raw | 2 | - | - |
| 5003 | getVidStream | String | 2 | Return a RTSP URL (rtsp://xxx.xxx.xxx.xxx) | HD: rtsp://xxx.xxx.xxx.xxx/av0_0 <br /> SD: rtsp://xxx.xxx.xxx.xxx/av0_1 |
| 5004 | getAudStream | Raw | 2 | - | - |
| 5005 | getPirDetection | Boolean | 2 | - | - |
| 5006 | getWifiMac | String | 2 | XX:XX:XX:XX:XX:XX | - |
| 5007 | getPushBtn | Boolean | 2 | - | - |
| 5008 | getTuyaPid | String | 2 | - | - |
| 5009 | getTuyaUuid | String | 2 | - | - |
| 5010 | getTuyaAuthkey | String | 2 | - | - |
| 5011 | getOtaStatus | Boolean | 2 | - | - |
| 5012 | getSdcardWritable | Boolean | 2 | - | - |
| 5013 | getLux | Integer | 2 | 0 ~ 5000 | - |
| 5014 | getSwannP2pId | String | 2 | - | - |
| 5015 | getSwannKey | Raw | 2 | - | - |
| 5016 | getSwannMac | String | 2 | xx:xx:xx:xx:xx:xx | - |
| 5017 | getSoundDB | Integer | 2 | 0 ~ 100 | - |
| 5018 | getDevInfo | String | 2 | Varies | - |

- *Set Operation*

| Code | Statement | Type | Access Type | Value/Comment | Default Value (Range) |
| :-: | :-: | :-: | :-: | :- | :-: |
| 6000 | setMicVol | Integer | 1 | 0 ~ 100 | - |
| 6001 | setSpeakerVol | Integer | 1 | 0 ~ 100 | - |
| 6002 | setAudStream | Raw | 1 | - | - |
| 6003 | setBlueLed | Boolean | 1 | - | - |
| 6004 | setRedLed | Boolean | 1 | - | - |
| 6005 | setFw | String | 1 | HTTP Url | - |
| 6006 | setMcuFw | String | 1 | HTTP Url | - |
| 6007 | setTuyaPid | String | 1 | - | - |
| 6008 | setTuyaUuid | String | 1 | - | - |
| 6009 | setTuyaAuthkey | String | 1 | - | - |
| 6010 | setTuyaDone | Boolean | 1 | - | - |
| 6011 | setLightOpen | Boolean | 1 | - | - |
| 6012 | setBrightness | Integer | 1 | - | - |
| 6013 | setSiren | Boolean | 1 | - | - |
| 6014 | setMcuDefaultVal | Boolean | 1 | - | - |
| 6015 | setIrCut | Boolean | 1 | - | - |
| 6016 | setIrLed | Boolean | 1 | - | - |
| 6017 | setGrayMode | Boolean | 1 | - | - |
| 6018 | setSystemCommand | String | 1 | - | - |
| 6019 | setLsAuthDone | Boolean | 1 | - | - |
| 6020 | setLightTemperature | Integer | 1 | - | - |
| 6021 | setWarningIndicator | Boolean | 1 | - | - |
| 6022 | setLuxTable | String | 1 | "1000,2803;900,2750;........." | - |
| 6023 | setSwannP2pId | String | 1 | - | - |
| 6024 | setSwannKey | Raw | 1 | - | - |
| 6025 | setSwannAuthDone | Boolean | 1 | - | - |
| 6026 | setSwannMac | String | 1 | xx:xx:xx:xx:xx:xx | - |
| 6027 | setEnablePirTest | Boolean | 1 | - | - |
| 6028 | setTime | String | 1 | - | 2022/07/05-08:01:02 |
| 6029 | setPicSize | int | 1 | - | 0: High, 1: Medium, 2: Low |
| 6030 | SetEnableMicSpeakerTest | Boolean | 1 | - | - |
| 6031 | SetQrcodeBlockTest | int | 1 | - | 1: 1x1, 2: 2x2, 3: 3x3 |


---

### App to Dev
  - Example: Set different types
    - App can send multiple setting by json object array
    - ```json
      {
        "cmd": 1, // Command number, refer to the title in this page
        "param": {
          "action": 1, // Refer to "Action" table
          "bunchSlots": [
            {
              "slot": 101, // Refer to "SLOT" table
              "type": 0, // Refer to "Type" table
              "boolVal": false
            },
            {
              "slot": 195, 
              "type": 1, 
              "intVal": 100
            },
            {
              "slot": 114, 
              "type": 2, 
              "strVal": "0800:1800"
            },
            {
              "slot": 184, 
              "type": 3,
              "enumVal": 0,
              "enumName": "LOW" // cloud should give the name of this value
            },
            {
              "slot": 115, 
              "type": 4,
              "rawVal":"dGVzdHN0cmluZ35+fn4=" // all raw data should be encoded to base64 format
            }
          ]
        },
        "token": 2147483647
      }
      ```
    - If there is no error occured, device will simply respond with rsp code
    - ```json
      {
        "cmd": 1,
        "param": {
        },
        "token": 2147483647,
        "rsp": 0
      }
      ```
    - If error occurs, device will respond an error code with error slots.
    - ```json
      {
        "cmd":1,
        "param": {
          "bunchSlots":[
            {
              "slot": 114
            },
            {
              "slot": 184
            }
          ]
        },
        "token": 2147483647,
        "rsp": 1
      }
      ```
  - Example: Get all slots
    - APP request action 2
    - ```json
      {
        "cmd": 1,
        "param": {
          "action": 2,
        },
        "token": 2147483647
      }
      ```
    - Device will respond all slots
    - ```json
      {
        "cmd": 1,
        "param": {
          "bunchSlots": [
            {
              "slot": 101,
              "type": 0,
              "boolVal": false
            },
            {
              "slot": 195, 
              "type": 1, 
              "intVal": 100
            },
            {
              "slot": 114, 
              "type": 2, 
              "strVal": "0800:1800"
            },
            {
              "slot": 184, 
              "type": 3,
              "enumVal": 0,
              "enumName": "LOW"
            },
            {
              "slot": 115, 
              "type": 4,
              "rawVal":"dGVzdHN0cmluZ35+fn4="
            }
          ]
        },
        "token": 2147483647,
        "rsp": 0
      }
      ```
  - Example: Get one slot
    - APP request action 0
    - ```json
      {
        "cmd": 1, 
        "param": {
          "action": 0,
          "singleSlot": {
            "slot": 101
          }
        },
        "token": 2147483647,
      }
      ```
    - Device will respond single slot
    - ```json
      {
        "cmd": 1, 
        "param": {
          "singleSlot": {
            "slot": 101,
            "type": 0,
            "boolVal": false
          }
        },
        "token": 2147483647,
        "rsp": 0
      }
      ```

