# 2018 - SetIrControl

### Command Number
| Command Nunber |
| :-: |
| 2018 |

### Host To Device
- Packet Format

Host To Device                          |     |     |     |     |
:-:                                     | :-: | :-: | :-: | :-: | -
Leading Byte | Type | Length | Data                       | End Byte          
0xFE         | 0    | Varies |  See Example Below         | 0xEF

- Example Data

```json
{
  "cmd": 2018,
  "data": {
    "ircut": true, // true: ON, false: OFF
    "irled": true,  // true: ON, false: OFF
    "grayMode": true  // true: ON, false: OFF
  }
}
```

***

### Device To Host
- Packet Format

Device To Host                          |     |     |     |     |
:-:                                     | :-: | :-: | :-: | :-: | -
Leading Byte | Type | Length | Data                       | End Byte          
0xFE         | 0    | Varies |  See Example Below         | 0xEF

- Example Data

```json
{
  "cmd": 2018,
  "data": {
    "resultType": 2,
    "result": "Success" // Or other description
  }
}
```