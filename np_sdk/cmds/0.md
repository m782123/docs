# 0 - Enable Media Channel

* [Response Table](intro.md#rsp_table)

---

### Data Channel Table 
| Code | Statement |
| :-:  | :- |
| 0 | Video Channel 0 (Default to device HD stream) |
| 1 | Video Channel 1 (Default to device SD stream) |
| 2 | Video Channel 2 (Default to SDcard video stream)|
| 3 | Video Channel 3 |
| 4 | Audio Channel 0 (Default to device audio stream) |
| 5 | Audio Channel 1 (Default to APP audio stream) |
| 6 | Audio Channel 2 (Default to SDcard audio stream)|
| 7 | Audio Channel 3 |
| 8 | Command Channel 0 |
| 9 | Command Channel 1 |

```C
typedef enum {
  NpDataChVid0 = 0, // Default to device HD stream
  NpDataChVid1,     // Default to device SD stream
  NpDataChVid2,     // Default to SDcard video stream
  NpDataChVid3,
  NpDataChAud0,     // Default to device audio stream
  NpDataChAud1,     // Default to APP audio stream
  NpDataChAud2,     // Default to SDcard audio stream
  NpDataChAud3,
  NpDataChCmd0,
  NpDataChCmd1,
  NpDataChMax
} NP_DATA_CH;
```

---

### App to Dev
  ```json
  {
    "cmd": 0, // Command number, refer to the title in this page
    "param": {
      "enable": true, // true: enable ch, false: disable ch
      "ch": 0 // Refer to data channel table
    },
    "token": 2147483647
  }
  ```

### Dev to APP
  ```json
  {
    "cmd": 0,
    "param": {},
    "token": 2147483647,
    "rsp": 0 // Refer to the response table
  }
  ```
