# Packets

- Packet Format

Byte  | 0            | 1                   | 2   | 3 | 4 | 5               | 6 + (Length - 1) | 6 + Length
:-:   | :-:          | :-:                 | :-: | - | - | -               | :-:              | :-: 
Name  | Leading Byte | Type                |  Length (Big Endian)    | | | | Data             | End Byte
Value | 0xFE         | Refer to Type Table | Varies                  | | | | Varies           | 0xEF 

- Type Table

Type Num | Description
:-:      | -
0        | Data is in JSON format
1        | Data is in raw format (bytes)