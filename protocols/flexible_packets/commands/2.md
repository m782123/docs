# 2 - Set Device Setting

### Command Types
- Old Style Command
  - Commands that used in 1858 project
- Extended Command
  - Commands that used in current projects
  - Click to see more. [Device Setting](../dev_setting/0.md)

### Command Number
| Command Nunber |
| :-:            |
| 2              |

### Host To Device
- Packet Format

Host To Device                          |     |     |     |     |
:-:                                     | :-: | :-: | :-: | :-: | -
Leading Byte | Type | Length | Data                       | End Byte          
0xFE         | 0    | Varies |  See Example Below         | 0xEF

- Example Data

```json
Old Style Command:
{
  "cmd": 2,
  "data": {
    "devSetting": "as87;",
    "commandID": "201910231112" // UTC format
  }
}

Extended Command:
{
  "cmd": 2,
  "data": {
    "devSetting": "nf0;",
    "commandID": "201910231112",
    "cmd": 0,
    "param": "CHT9210,0916817088"
  }
}
```

***

### Device To Host
- Packet Format

Device To Host                          |     |     |     |     |
:-:                                     | :-: | :-: | :-: | :-: | -
Leading Byte | Type | Length | Data                       | End Byte          
0xFE         | 0    | Varies |  See Example Below         | 0xEF

- Example Data

```json
Old Style Command:
{
  "cmd": 2,
  "data": {
    "devSetting": "as87;",
    "result": "1as870000;",
    "commandID": "20191023111200"
  }
}

Extended Command:
{
  "cmd": 2,
  "data": {
    "devSetting": "nf0;",
    "result": "1nf00000;",
    "commandID": "20191023111200",
    "cmd": 0,
    "param": ""
  }
}
```