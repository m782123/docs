# Cloud Recording

### Command Table 
| Code | Statement |
| :-:  | :- |
| 0 | Get Dates |
| 1 | Get Evevtt List |
| 2 | Play Stream |
| 3 | Stop Stream |
| 4 | Delete Events |
| 5 | Delete All Events |

### Encryption
- Images & videos are all encrypted by AES128 CBC
- The key for the encryption is offered by cloud
- IV should be 
  - ```C
    0xea, 0x67, 0x2b, 0x9d, 0x95, 0xe2, 0xb8, 0x4f, 0x02, 0xd9, 0xa8, 0x17, 0xed, 0xeb, 0x55, 0x51
    ```

### Defines
  - NP_CLOUD_REC_HANDLE
    - ```C
      typedef void* NP_CLOUD_REC_HANDLE;
      ```
    - Return once user calls
  - NP_CLOUD_REC_PLAY_STREAM_CB
    - ```C
      typedef void (* NP_CLOUD_REC_PLAY_STREAM_CB) (NP_FRAME_TYPE type, uint8_t *buf, uint32_t buf_len);
      ```
    - Callback for play cloud video
  - NP_CLOUD_REC_EVT_TYPE
    - ```C
      typedef enum {
        NpCloudRecEvtTypePushBtn = 1,
        NpCloudRecEvtTypeMotionDetection,
        NpCloudRecEvtTypePushAndMotion = 5,
        NpCloudRecEvtTypeTimerRecording = 6,
      } NP_CLOUD_REC_EVT_TYPE;
      ```
  - np_cloud_rec_init ()
    - ```C
      typedef union {
        struct {
          char                        app[52];
          char                        uid[52];
          char                        token[512];
          char                        did[52];
          int                         time_offset;
          NP_CLOUD_REC_PLAY_STREAM_CB play_cb;
        } app;
        struct {
          char                        private_key[40];
        } dev;
      } NP_CLOUD_REC_PARAM;

      NP_CLOUD_REC_HANDLE
      np_cloud_rec_init (
        NP_CLOUD_REC_PARAM *param
        );
      ```
    - User needs to call this API first to initialize the environment variables
    - User can create multiple handles for different devices
    - Parameters
      - app
        - app key
      - did
        - target device id
      - token
        - A string provided by cloud after logining
      - uid
        - A string provided by cloud after logining
      - time_offset
        - Current time offset, for example, Taiwan is 8 (UTC+8)
      - play_cb
        - Callback for play cloud video
  - np_cloud_rec_exe_json_cmd ()
    - ```C
      int
      np_cloud_rec_exe_json_cmd (
        NP_CLOUD_REC_HANDLE     handle,
        char                   *json_str
        );
      ```
    - API for executing command. User should put a json format string to this API
  - np_cloud_rec_destroy_handle ()
    - ```C
      void
      np_cloud_rec_destroy_handle (
        NP_CLOUD_REC_HANDLE  handle
        );
      ```
    - User must destroy the handle once exit.

