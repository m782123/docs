# 2005 - SetMcuFw

### Command Number
| Command Nunber |
| :-: |
| 2005 |

### Host To Device
- Packet Format

Host To Device                          |     |     |     |     |
:-:                                     | :-: | :-: | :-: | :-: | -
Leading Byte | Type | Length | Data                       | End Byte          
0xFE         | 0    | Varies |  See Example Below         | 0xEF

- Example Data

```json
{
  "cmd": 2005,
  "data": {
    "paramType": 2,
    "param": "http://xxx.xxx.xxx:xxx/fw" // http URL
  }
}
```

***

### Device To Host
- Packet Format

Device To Host                          |     |     |     |     |
:-:                                     | :-: | :-: | :-: | :-: | -
Leading Byte | Type | Length | Data                       | End Byte          
0xFE         | 0    | Varies |  See Example Below         | 0xEF

- Example Data

```json
{
  "cmd": 2005,
  "data": {
    "resultType": 2,
    "result": "Success" // Or other description
  }
}
```