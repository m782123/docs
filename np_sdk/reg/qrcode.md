# QR Code

### APP
  - Step 1
    - APP should generate a QR code that contains the [message](intro.md#message), then put it in front of the camera for recognition.
  - Step 2
    - APP should poll poll the cloud to check the registration status of it.