# Registration

### Registration mode: {#id=reg_mode}
  * [Airtouch](airtouch.md)
  * [AP](ap.md)
  * [QR Code](qrcode.me)

### Message {#id=message}
All these modes shold transfer same message below to device for registering to the cloud:
```json
{
  "s": "CHT9210", // SSID
  "p": "0916817088", // password for SSID
  "t": "Axf2aVhywq" // token from cloud
}
```

### Response from device in cloud data store
Once device connects to the internet and registers to the cloud, device will store information in cloud data store. 
```json
{
    "msg":"{\\\"accesspw\\\": \\\"xxxxx\\\",\\\"pid\\\": \\\"xxxxx\\\"}",
    "state": "sucess" // success / error
}
```
