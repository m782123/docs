# PC223

  * [SetGet Type Table](../intro.md#setget_table)

### Common SLOT Table
| Code | Statement | Type | Access Type | Value | Default Value |
| :-: | :-: | :-: | :-: | :- | :-: |
| 101 | Light | Boolean | 0 | - | ON |
| 103 | Flip | Boolean | 0 | - | OFF |
| 104 | Watermark | Boolean | 0 | - | ON |
| 106 | AlarmSensitivity | Enum | 0 | 0: LOW <br /> 1: MID <br /> 2: HIGH | 2 |
| 108 | NightMode | Enum | 0 | 0: OFF <br /> 1: ON | 0 |
| 109 | SdcardStorageOnlyGet | String | 2 | Total/Used/Free | 0/0/0 |
| 110 | SdcardStatusOnlyGet | Integer | 2 | 1: normal <br /> 2: abnormal <br /> 3: no space <br /> 4: formating <br /> 5: no SD | 5 |
| 111 | SdcardFormat | Boolean | 0 | - | OFF |
| 112 | SdcardUnmount | Boolean | 0 | - | OFF |
| 150 | SdcardRecordEnable | Boolean | 0 | - | ON |
| 151 | SdcardRecordMode | Enum | 0 | 0: Record By Event<br /> 1: Record Continuously | 0 |
| 162 | DeviceReboot | Boolean | 1 | - | OFF |
| 168 | MotionZoneSwitch | Boolean | 0 | - | ON |
| 169 | MotionZoneArea | String | 0 | {\\\\\\"num:1\\\\\\",\\\\\\"region0\\\\\\":{\\\\\\"x\\\\\\":0, \\\\\\"y\\\\\\":0, \\\\\\"xlen\\\\\\":50,\\\\\\"ylen\\\\\\":50}} | {\\\\\\"num:1\\\\\\",\\\\\\"region0\\\\\\":{\\\\\\"x\\\\\\":0, \\\\\\"y\\\\\\":0, \\\\\\"xlen\\\\\\":100,\\\\\\"ylen\\\\\\":100}} |
| 181 | PIR1Switch | Boolean | 0 | - | ON |
| 184 | PIRSensitivity | Enum | 0 | 0: LOW <br /> 1: MID <br /> 2: HIGH | 0 |
| 256 | FactoryReset | Boolean | 0 | - | OFF |
| 257 | DevOtaStart | Boolean | 0 | - | OFF |
| 260 | GetFwVer | String | 2 | - | 0.0.0 |
| 261 | GetVidChannelInfo | String | 2 | Channel/Width/Height/FPS;... <br /> 0/1920/1080/20;1/640/360/15; | - |
| 262 | WifiSignalStrength | Integer | 2 | - | 0 (0~100) |
| 267 | SetGetSdpInfo | String | 0 | Set: APP/Cloud will provide SDP info <br /> Get: Device should provide self SDP info | - |
| 274 | Brightness | Integer | 0 | 0 ~ 100 | - |
| 288 | WIFI SSID | String | 2 | - | - |
| 289 | SetOffer | String | 1 | - | - |
| 290 | SetCandidate | String | 1 | - | - |
| 291 | ReportCandidate | String | 2 | - | 0 |

---
