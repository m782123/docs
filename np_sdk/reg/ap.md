# AP mode

App should call the following APIs:
```C
char *
np_reg_ap_mode_send (
  char *dev_ip,
  char *ssid,
  char *pwd,
  char *token
);
```

### APP
  - Step 1 : Call ```np_reg_ap_mode_send ()```
    - This API will auto-generate the [message](intro.md#message). APP dose not need to bother that.
    - dev_ip
      - Device IP, it should be 192.168.100.1 
    - ssid
      - SSID of wifi
    - pwd
      - password of SSID
    - token
      - string from cloud
  - Step 2
    - If the device responses ```{"rsp":0}```, APP should poll poll the cloud to check the registration status of it.