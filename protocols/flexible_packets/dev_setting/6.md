# 6 - SetVideoWatermark

### NF Command Number
- <span style="color:red">**All packets should be transformed into base64 format.**</span>

| NF Command Nunber |
| :-:|
| 6 |

### Host To Device
- Packet Format

Host To Device                          |     |     |     |     |
:-:                                     | :-: | :-: | :-: | :-: | -
Leading Byte | Type | Length | Data                       | End Byte          
0xFE         | 0    | Varies |  See Example Below         | 0xEF

- Example Data

```json
{
  "cmd": 2,
  "data": {
    "devSetting": "nf0;",
    "commandID": "201910231112",
    "nfCmd": 6,
    "nfParam": {
      "watermark": true
    }
  }
}
```

***

### Device To Host
- Packet Format

Device To Host                          |     |     |     |     |
:-:                                     | :-: | :-: | :-: | :-: | -
Leading Byte | Type | Length | Data                       | End Byte          
0xFE         | 0    | Varies |  See Example Below         | 0xEF

- Example Data

```json
{
  "cmd": 2,
  "data": {
    "devSetting": "nf0;",
    "result": "1nf00000;",
    "commandID": "20191023111200",
    "nfCmd": 6,
    "nfParam": "",
    "nfCmdResult": 0,
    "nfCmdResultStr": "Success"
  }
}
```