# VX21113

  * [SetGet Type Table](../intro.md#setget_table)

### Customized SLOT Table
| Code | Statement | Type | Access Type | Value | Default Value |
| :-: | :-: | :-: | :-: | :- | :-: |
| 500 | Mode | Enum | 0 | 0: TEST <br /> 1: AUTO <br /> 2: PC <br /> 3: Lifestyle_LDR <br /> 4: Lifestyle_Clock | 0 |
| 501 | Trigger | Enum | 0 | 0: LDR_NIGHT <br /> 1: LDR_DAY <br /> 2: PIR | 2 |
| 502 | WIFI SSID | String | 2 | - | - |
| 503 | Lifestyle Duration | Integer | 0 | 1 - 12 <br /> Unit: Hour | 1 |
| 504 | Detected Duration | Integer | 0 | 30 - 1200 <br /> Unit: Second | 30 |
| 505 | Lifestyle Clock | String | 0 | s=18,e=6 | s=18,e=6 |
| 506 | High Brightness | Integer | 0 | 50 - 100 <br /> Unit: % | 100 |
| 507 | Low Brightness | Integer | 0 | 0 - 50 <br /> Unit: % | 30 |
| 508 | High Color Temprature | Integer | 0 | 2200 - 5000 <br /> Unit: K | 5000 |
| 509 | LOw Color Temprature | Integer | 0 | 2200 - 5000 <br /> Unit: K | 2200 |
| 510 | MCU Version | String | 2 | - | 0.0.0 |
| 511 | LDR Switch | Boolean | 0 | - | ON |
| 512 | LDR State | Boolean | 2 | - | OFF |
| 513 | Light State | Boolean | 2 | - | OFF |
| 514 | Light Brighness | Integer | 0 | 0 - 100 <br /> Unit: % | 100 |
| 515 | Light Temprature | Integer | 0 | 2200 - 5000 <br /> Unit: K | 5000 |
| 516 | Offline Grouping | Boolean | 0 | - | OFF |
| 517 | Detected Sound | Enum | 0 | 0: OFF <br /> 1: Default (need to send slot 259) <br /> 2: Bark <br /> 3: Custom (need to send slot 258) | 0 |
| 518 | Sensitivity By Feet | Integer | 0 | 5 - 70 <br /> Unit: feet | 35 |
| 519 | Turn On/Off light | Boolean | 1 | - | MCU default |
| 520 | Brightness Adjustment | Integer | 1 | -100 - 100 <br /> Unit: % <br /> Range: 0 - 100 | MCU default |
| 521 | Light Temperature Adjustment | Integer | 1 | -1000 - 1000 <br /> Unit: K <br /> Range: 2200 - 5000 | MCU default |
| 522 | Set Group Attribute | String | 1 | Range: <br />0000000100000000 - FFFFFFFFFFFFFFFF | - |

---

### Example JSON from cloud
```json
{
  "pid":"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
  "availableSlots": [
    {
      "slot": 101,
      "type": 0,
      "setGet": 0,
      "notes": "Yo~~~"
    },
    {
      "slot": 103,
      "type": 0,
      "setGet": 0,
      "notes": "Yo~~~"
    },
    {
      "slot": 104,
      "type": 0,
      "setGet": 0,
      "notes": "Yo~~~"
    },
    {
      "slot": 106,
      "type": 3,
      "setGet": 0,
      "name":[
        "LOW",
        "MEDIUM",
        "HIGH"
      ],
      "notes": "Yo~~~"
    },
    {
      "slot": 108,
      "type": 3,
      "setGet": 0,
      "name":[
        "OFF",
        "ON"
      ],
      "notes": "Yo~~~"
    },
    {
      "slot": 109,
      "type": 2,
      "setGet": 2,
      "notes": "3503775744|778977280|2718158848" // Total|Used|Free
    },
    {
      "slot": 110,
      "type": 1,
      "setGet": 2,
      "rangeMin": 1,
      "rangeMax": 5,
      "gap": 1,
      "unit": "",
      "notes": "1: normal, 2:abnormal, 3:no space, 4: formating, 5: no SD"
    },
    {
      "slot": 111,
      "type": 0,
      "setGet": 0,
      "notes": "Yo~~~"
    },
    {
      "slot": 115,
      "type": 4,
      "setGet": 2,
      "notes": "Yo~~~"
    },
    {
      "slot": 117,
      "type": 1,
      "setGet": 2,
      "rangeMin": -20000,
      "rangeMax": 200000,
      "gap": 1,
      "unit": "",
      "notes": "-2000: formatting, -2001: abnormal, -2002: no sdcard, -2003: sdcasrd error"
    },
    {
      "slot": 134,
      "type": 0,
      "setGet": 0,
      "notes": "Yo~~~"
    },
    {
      "slot": 150,
      "type": 0,
      "setGet": 0,
      "notes": "Yo~~~"
    },
    {
      "slot": 151,
      "type": 3,
      "setGet": 0,
      "name":[
        "Record By Event",
        "Record Continuously"
      ],
      "notes": "Yo~~~"
    },
    {
      "slot": 159,
      "type": 0,
      "setGet": 0,
      "notes": "Yo~~~"
    },
    {
      "slot": 162,
      "type": 0,
      "setGet": 0,
      "notes": "Yo~~~"
    },
    {
      "slot": 168,
      "type": 0,
      "setGet": 0,
      "notes": "Yo~~~"
    },
    {
      "slot": 169,
      "type": 2,
      "setGet": 0,
      "notes": "{\\\"num:1\\\",\\\"region0\\\":{\\\"x\\\":0, \\\"y\\\":0, \\\"xlen\\\":50,\\\"ylen\\\":50}}"
    },
    {
      "slot": 181,
      "type": 0,
      "setGet": 0,
      "notes": "Yo~~~"
    },
    {
      "slot": 184,
      "type": 0,
      "setGet": 0,
      "name":[
        "LOW",
        "MEDIUM",
        "HIGH"
      ],
      "notes": "Yo~~~"
    },
    {
      "slot": 256,
      "type": 0,
      "setGet": 0,
      "notes": "Yo~~~"
    },
    {
      "slot": 257,
      "type": 0,
      "setGet": 0,
      "notes": "Yo~~~"
    },
    {
      "slot": 500,
      "type": 3,
      "setGet": 0,
      "name":[
        "TEST",
        "AUTO",
        "PC",
        "Lifestyle_LDR",
        "Lifestyle_Clock"
      ],
      "notes": "Yo~~~"
    },
    {
      "slot": 501,
      "type": 3,
      "setGet": 0,
      "name":[
        "LDR_NIGHT",
        "LDR_DAY",
        "PIR"
      ],
      "notes": "Yo~~~"
    },
    {
      "slot": 502,
      "type": 2,
      "setGet": 2,
      "notes": "CHT9210"
    },
    {
      "slot": 503,
      "type": 1,
      "setGet": 0,
      "rangeMin": 1,
      "rangeMax": 12,
      "gap": 1,
      "unit": "Hour",
      "notes": "Yo~~~"
    },
    {
      "slot": 504,
      "type": 1,
      "setGet": 0,
      "rangeMin": 30,
      "rangeMax": 1200,
      "gap": 1,
      "unit": "Second",
      "notes": "Yo~~~"
    },
    {
      "slot": 505,
      "type": 2,
      "setGet": 0,
      "notes": "s=18,e=6"
    },
    {
      "slot": 506,
      "type": 1,
      "setGet": 0,
      "rangeMin": 50,
      "rangeMax": 100,
      "gap": 1,
      "unit": "%",
      "notes": "Yo~~~"
    },
    {
      "slot": 507,
      "type": 1,
      "setGet": 0,
      "rangeMin": 0,
      "rangeMax": 50,
      "gap": 1,
      "unit": "%",
      "notes": "Yo~~~"
    },
    {
      "slot": 508,
      "type": 1,
      "setGet": 0,
      "rangeMin": 2200,
      "rangeMax": 5000,
      "gap": 1,
      "unit": "K",
      "notes": "Yo~~~"
    },
    {
      "slot": 509,
      "type": 1,
      "setGet": 0,
      "rangeMin": 2200,
      "rangeMax": 5000,
      "gap": 1,
      "unit": "K",
      "notes": "Yo~~~"
    },
    {
      "slot": 510,
      "type": 2,
      "setGet": 2,
      "notes": "1.0.0"
    },
    {
      "slot": 511,
      "type": 0,
      "setGet": 0,
      "notes": "Yo~~~"
    },
    {
      "slot": 512,
      "type": 0,
      "setGet": 0,
      "notes": "Yo~~~"
    },
    {
      "slot": 513,
      "type": 0,
      "setGet": 0,
      "notes": "Yo~~~"
    },
    {
      "slot": 514,
      "type": 1,
      "setGet": 0,
      "rangeMin": 0,
      "rangeMax": 100,
      "gap": 1,
      "unit": "%",
      "notes": "Yo~~~"
    },
    {
      "slot": 515,
      "type": 1,
      "setGet": 0,
      "rangeMin": 2200,
      "rangeMax": 5000,
      "gap": 1,
      "unit": "K",
      "notes": "Yo~~~"
    },
    {
      "slot": 516,
      "type": 0,
      "setGet": 0,
      "notes": "Yo~~~"
    }
  ]
}
```