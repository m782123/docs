# Type 0

### Related Projects
- V100CW20
- V200CW21
- PAC027

### Checksum {#id=checksum}
- Sum of whole packet.

### DP Data Type {#id=dp}
Name   | Type
:-:    | :-:
RAW    | 0x00
Bool   | 0x01
Value  | 0x02
String | 0x03
Enum   | 0x04
Bitmap | 0x05