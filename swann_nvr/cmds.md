# CMDs

| Response code | Statement |
| :-:  | :- |
| 0 | Success |
| 1 | Invalid Method |
| 2 | Invalid Data |
| 3 | Invalid Command |
| 4 | Not Found |

---

| Command | Statement |
| :-:  | :- |
| 0 | [Serch Devices](cmds.md#cmd0) |
| 1 | [Connect](cmds.md#cmd1) |
| 2 | [Add device to channel](cmds.md#cmd2) |
| 3 | [List channels](cmds.md#cmd3) |
| 4 | [Delete channel](cmds.md#cmd4) |

## 0 - Serch Devices {#id=cmd0}
- Web send
```json
{
  "cmd": 0
}
```

- Dev respond
```json
{
  "cmd": 0,
  "data": {
    "devs": [ // string array
      {
        "devIp": "192.168.1.x",
        "mac": "aa:bb:cc:dd:ee:ff",
        "fwVer": "1.0.0",
        "port": 554,
        "manufacturer": "LivingStyle",
        "devType": "xxxxxxx"
      }
    ]
  },
  "rsp": 0
}
```
---

## 1 - Connect {#id=cmd1}
- Web send
```json
{
  "cmd": 1,
  "data": {
    "ch": 1,
    "sdp_offer": "SDP info"
  }
}
```

- Dev respond
```json
{
  "cmd": 1,
  "data": {
    "ch": 1,
    "sdp_answer": "SDP info"
  },
  "rsp": 0
}
```
---

## 2 - Add device to channel {#id=cmd2}
- Web send
```json
{
  "cmd": 2,
  "data": {
    "devIp": "192.168.1.x",
    "ch": 1,
    
    // section below is optional 
    "devName": "xxxx",
    "port": 1234,
    "protocol": 1, // 0 - onvif, 1 - private
    "userName": "xxxxx",
    "password": "xxxxx"
  }
}
```

- Dev respond
```json
{
  "cmd": 2,
  "data": {
  },
  "rsp": 0
}
```
---

## 3 - List channels {#id=cmd3}
- Web send
```json
{
  "cmd": 3
}
```

- Dev respond
```json
{
  "cmd": 3,
  "data": {
    "chs": [
      {
        "ch": 1,
        "empty": false,
        "state": 1, // 0 - offline, 1 - online
        "devIp": "192.168.1.x",
        "mac": "aa:bb:cc:dd:ee:ff",
        "fwVer": "1.0.0",
        "port": 554,
        "manufacturer": "LivingStyle",
        "devType": "xxxxxxx"
      },
      {
        "ch": 2,
        "empty": true,
        "state": 0, // 0 - offline, 1 - online
      }
    ]
  },
  "rsp": 0
}
```
---

## 4 - Delete channel {#id=cmd4}
- Web send
```json
{
  "cmd": 4,
  "data": {
    "ch": 1
  }
}
```

- Dev respond
```json
{
  "cmd": 4,
  "data": {
  },
  "rsp": 0
}
```
---
