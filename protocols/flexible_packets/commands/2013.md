# 2013 - SetSiren

### Command Number
| Command Nunber |
| :-: |
| 2013 |

### Host To Device
- Packet Format

Host To Device                          |     |     |     |     |
:-:                                     | :-: | :-: | :-: | :-: | -
Leading Byte | Type | Length | Data                       | End Byte          
0xFE         | 0    | Varies |  See Example Below         | 0xEF

- Example Data

```json
{
  "cmd": 2013,
  "data": {
    "siren": true // true:on false:off
  }
}
```

***

### Device To Host
- Packet Format

Device To Host                          |     |     |     |     |
:-:                                     | :-: | :-: | :-: | :-: | -
Leading Byte | Type | Length | Data                       | End Byte          
0xFE         | 0    | Varies |  See Example Below         | 0xEF

- Example Data

```json
{
  "cmd": 2013,
  "data": {
    "resultType": 2,
    "result": "Success" // Or other description
  }
}
```