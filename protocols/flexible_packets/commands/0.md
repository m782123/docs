# 0 - Get Media List

### Command Number
| Command Nunber |
| :-:            |
| 0              |

### Host To Device
- Packet Format

Host To Device                          |     |     |     |     |
:-:                                     | :-: | :-: | :-: | :-: | -
Leading Byte | Type | Length | Data                       | End Byte          
0xFE         | 0    | Varies |  See Example Below         | 0xEF

- Example Data

```json
{
  "cmd": 0,
  "data": {
    
  }
}
```

***

### Device To Host
- Packet Format

Device To Host                          |     |     |     |     |
:-:                                     | :-: | :-: | :-: | :-: | -
Leading Byte | Type | Length | Data                       | End Byte          
0xFE         | 0    | Varies |  See Example Below         | 0xEF

- Example Data

```json
{
    "cmd": 0,
    "data": {
        "mediaList": [
            {
                "name": "test0.jpg",
                "size": 12345,
                "date": "2019-10-10 12:13:14"
            },
            {
                "name": "test1.jpg",
                "size": 123456,
                "date": "2019-10-11 12:13:14"
            },
            {
                "name": "test2.avi",
                "size": 1234567,
                "date": "2019-10-12 12:13:14"
            }
        ],
        "timezone": 8,
        "timezoneName": "Asia/Taipei"
    }
}
```