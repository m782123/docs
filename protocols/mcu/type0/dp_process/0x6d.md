# 0x6D - Motion Alert

### Device To MCU

DP Num | [Data Type](../type.md#dp) | Length           | | Parameter    | | 
:-:    | :-:                        | :-:      | :-:     | :-:            | :-
0      | 1                          | 2        | 3       | 4            | |
0x6D   | 0x01                       | 0x00     | 0x01    | 0x00           | OFF
^      | ^                          | ^        | ^       | 0x01           | ON

### MCU To Device
- Same as above