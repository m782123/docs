# PAC031 / PAC271

  * [SetGet Type Table](../intro.md#setget_table)

### Common SLOT Table
| Code | Statement | Type | Access Type | Value | Default Value |
| :-: | :-: | :-: | :-: | :- | :-: |
| 101 | Light | Boolean | 0 | - | ON |
| 103 | Flip | Boolean | 0 | - | OFF |
| 104 | Watermark | Boolean | 0 | - | ON |
| 106 | AlarmSensitivity | Enum | 0 | 0: LOW <br /> 1: MID <br /> 2: HIGH | 2 |
| 109 | SdcardStorageOnlyGet | String | 2 | Total/Used/Free | 0/0/0 |
| 110 | SdcardStatusOnlyGet | Integer | 2 | 1: normal <br /> 2: abnormal <br /> 3: no space <br /> 4: formating <br /> 5: no SD | 5 |
| 111 | SdcardFormat | Boolean | 0 | - | OFF |
| 112 | SdcardUnmount | Boolean | 0 | - | OFF |
| 150 | SdcardRecordEnable | Boolean | 0 | - | ON |
| 151 | SdcardRecordMode | Enum | 0 | 0: Record By Event<br /> 1: Record Continuously | 0 |
| 159 | SirensSound | Boolean | 0 | - | ON |
| 162 | DeviceReboot | Boolean | 1 | - | OFF |
| 168 | MotionZoneSwitch | Boolean | 0 | - | ON |
| 169 | MotionZoneArea | String | 0 | {\\\\\\"num:1\\\\\\",\\\\\\"region0\\\\\\":{\\\\\\"x\\\\\\":0, \\\\\\"y\\\\\\":0, \\\\\\"xlen\\\\\\":50,\\\\\\"ylen\\\\\\":50}} | {\\\\\\"num:1\\\\\\",\\\\\\"region0\\\\\\":{\\\\\\"x\\\\\\":0, \\\\\\"y\\\\\\":0, \\\\\\"xlen\\\\\\":100,\\\\\\"ylen\\\\\\":100}} |
| 181 | PIR1Switch | Boolean | 0 | - | ON |
| 184 | PIRSensitivity | Enum | 0 | 0: LOW <br /> 1: MID <br /> 2: HIGH | 0 |
| 256 | FactoryReset | Boolean | 0 | - | OFF |
| 257 | DevOtaStart | Boolean | 0 | - | OFF |
| 260 | GetFwVer | String | 2 | - | 0.0.0 |
| 261 | GetVidChannelInfo | String | 2 | Channel/Width/Height/FPS;... <br /> 0/1920/1080/20;1/640/360/15; | - |
| 262 | WifiSignalStrength | Integer | 2 | - | 0 (0~100) |
| 267 | SetGetSdpInfo | String | 0 | Set: APP/Cloud will provide SDP info <br /> Get: Device should provide self SDP info | - |
| 288 | WIFI SSID | String | 2 | - | - |

---

### Customized SLOT Table
| Code | Statement | Type | Access Type | Value | Default Value |
| :-: | :-: | :-: | :-: | :- | :-: |
| 500 | Mode | Enum | 0 | 0: TEST <br /> 1: AUTO <br /> 2: MANUAL_ON <br /> 3: MANUAL_OFF <br /> 4: TIME | 2 |
| 501 | Detected Duration | Integer | 0 | 20 - 900 <br /> Unit: Second | 20 |
| 506 | Warning Indicator | Boolean | 0 | - | OFF |
| 507 | LUX | Integer | 2 | - | Varies |
| 508 | Detected Sound | Boolean | 0 | - | OFF |
| 509 | D2D Duration | Integer | 0 | 1 - 12 <br /> Unit: Hour | 12 |
| 510 | Warning Indicator Manual | Boolean | 0 | - | OFF |
| 511 | Mode In Auto | Enum | 0 | 0: OFF <br /> 1: TIME <br /> 2: D2D | 0 |
| 512 | Custom LUX | Enum | 0 | 0: Day <br /> 1: Dusk <br /> 2: Night | 2 |

---
