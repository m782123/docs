# 0x67 - On Time

### Device To MCU

DP Num | [Data Type](../type.md#dp) | Length           | | Parameter  | | 
:-:    | :-:                        | :-:      | :-:     | :-:          | :-
0      | 1                          | 2        | 3       | 4 ~ 7      | |
0x67   | 0x02                       | 0x00     | 0x04    | 0x00         | Test
^      | ^                          | ^        | ^       | 0x01 ~ 0x384 | 1 ~ 900 second

### MCU To Device
- Same as above