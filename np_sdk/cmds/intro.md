# Commands

### Response Table {#id=rsp_table}
| Code | Statement |
| :-:  | :- |
| 0 | Success |
| 1 | Invalid Parameters |
| 2 | Not Found |
| 3 | Buffer Full |

### Basic Properties
  - APP Send
    - ```json
      {
        "cmd": 0, // command number may different
        "param": {
        },
        "token": 2147483647 // 0 ~ 2147483647
      }
      ```
  - Device Response
    - ```json
      {
        "cmd": 0, // command number may different
        "param": {
        },
        "token": 2147483647,
        "rsp": 0 // refer to response table
      }
      ```