# Swann NVR

## HTTP URL
- http://deviceip:8000

## Restful API
- http://deviceip:8001/api/cmd
- Request method : POST

## Basic json format
```json
{
  "cmd": 0, // command
  "data": {
    // command-specific data content
  },
  "rsp": 0 // response
}
```