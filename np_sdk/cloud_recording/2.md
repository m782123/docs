# 2 - Play Stream

- Once the command is executed, SDK will try to get the video file from the cloud. If the video file is downloaded and decrypted, SDK will send video / audio frame to the callback that user registered by np_cloud_rec_init ()
- Only one event id will be played. Multiple-play is not supported.

### Example for APP send
```json
{
  "cmd": 2, // command
  "evtId": 57755, // event id
  "token": 123456789
}
```

### Example for response
```json
{
  "result": 0, // 0: success, -1: error
  "cmd": 2, // command
  "did": "KYHPBCSMO27QCF4IDMBR6GNUGITKK43K", // device that user requested
  "token": 123456789
}
```